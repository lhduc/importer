<?php
define('ROOTPATH', __DIR__);
define('LIBPATH', ROOTPATH . '/libs/');
define('FILEPATH', ROOTPATH . '/files/');
define('PAGEPATH', ROOTPATH . '/pages/');

define('DB_NAME', 'importer');
define('DB_HOST', 'localhost');
define('DB_USR', 'root');
define('DB_PWD', 'root');

define('MODE_PRIMARY', 'primary');
define('MODE_SUCCESS', 'success');
define('MODE_ERROR', 'danger');