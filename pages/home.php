<?php
require_once(LIBPATH . 'PHPExcel/PHPExcel.php');

$debug = false;
if (isset($_POST["upload"])) {
  if (!empty($import_period = $_POST['import_period'])) {
    $import_period = '01-' . $import_period;
    $import_period = date('Y-m-01', strtotime($import_period));
    
    if ($debug) {
      // Show benchmark
      require_once(LIBPATH . 'appgati.class.php');
      $app = new AppGati();
      $app->Step('1');
      import($import_period);
      $app->Step('2');
      $report = $app->Report('1', '2');
      echo '<pre>';print_r($report);echo '</pre>';
    } else {
      // Without showing benchmark
      import($import_period);
    }
  } else {
    error('Bắt buộc nhập chu kỳ.');
  }
}

function import($import_period) {
  $mysqli = MyDB::getInstance()->getConnection();

  // Upload file
  $upload_dir = FILEPATH . '/public/';
  $file_path = $upload_dir . '/' . generate_random_string() . '_' . $_FILES['doc']['name'];
  $upload = upload_file($file_path);
  if ($upload === true) {
    $mysqli->query("START TRANSACTION");
    $mysqli->query('TRUNCATE TABLE `import`');
    
    // Read file
    $objPHPExcel = PHPExcel_IOFactory::load($file_path);
    foreach ($objPHPExcel->getWorksheetIterator() as $k => $worksheet) {
      // Skip first sheet
      if ($k == 0) continue;
        
      $col1 = strtolower(trim($worksheet->getCellByColumnAndRow(1, 1)->getValue()));
      $col2 = strtolower(trim($worksheet->getCellByColumnAndRow(2, 1)->getValue()));
      $col3 = strtolower(trim($worksheet->getCellByColumnAndRow(3, 1)->getValue()));
      $col4 = strtolower(trim($worksheet->getCellByColumnAndRow(4, 1)->getValue()));
      $col5 = strtolower(trim($worksheet->getCellByColumnAndRow(5, 1)->getValue()));
      $col6 = strtolower(trim($worksheet->getCellByColumnAndRow(6, 1)->getValue()));

      $worksheet_title = $worksheet->getTitle();
      $highest_row = $worksheet->getHighestRow();

      // Check if sheet columns are valid
      $noPeriod = false;
      if ($col1 != strtolower('So TB') || $col2 != strtolower('Ten TB') 
      || $col3 != strtolower('Dia Chi') || $col4 != strtolower('MST')) {
        if ($col5 == strtolower('Số Tiền')) {
          $noPeriod = true;
        } elseif ($col5 != 'CK' && $col6 != 'Số Tiền') {
          info("Sheet {$worksheet_title}: Cấu trúc cột không đúng.", MODE_ERROR);
          continue;
        }
      }

      $count = 0;
      for ($row = 3; $row <= $highest_row; $row++) {
        $i = 1;
        $phone_no = trim($worksheet->getCellByColumnAndRow($i++, $row)->getValue());
        $owner = trim($worksheet->getCellByColumnAndRow($i++, $row)->getValue());
        $address = trim($worksheet->getCellByColumnAndRow($i++, $row)->getValue());
        $tax_code = trim($worksheet->getCellByColumnAndRow($i++, $row)->getValue());
        if ($noPeriod) {
          $period = 1;
          $amount = trim($worksheet->getCellByColumnAndRow($i++, $row)->getValue());
        } else {
          $period = trim($worksheet->getCellByColumnAndRow($i++, $row)->getValue());
          $amount = trim($worksheet->getCellByColumnAndRow($i++, $row)->getValue());
        }

        // If number, name or amount is empty, skip row
        if (empty($phone_no) || empty($owner) || empty($amount)) {
          continue;
        }

        $query = "INSERT DELAYED INTO `import` (phone_no, owner, address, tax_code, period, amount, `date`)
                VALUES (
                  '" . escape($phone_no) . "',
                  '" . escape($owner) . "',
                  '" . escape($address) . "',
                  '" . escape($tax_code) . "',
                  '" . escape($period) . "',
                  '" . escape($amount) . "',
                  '" . escape($import_period) . "'
                )";
        $result = $mysqli->query($query);
        if ($result) $count++;
        else info("Sheet {$worksheet_title}: Không thể lưu dữ liệu của dòng $row.", MODE_ERROR);
      }

      if ($count) {
        info("Sheet $worksheet_title: Đã xử lý $count / " . ($highest_row - 3) . " dòng.");
      }
    }

    $mysqli->query("COMMIT");
  } else {
    error($upload);
  }
}

?>

<div id="page-import" class="row">
  <div class="col-md-12">
    <h1 class="text-primary">Nhập dữ liệu</h1>
    <?=display_msg()?>
    <form action="" method="post" enctype="multipart/form-data">
      <div class="panel panel-default">
        <div class="panel-body">
          <div class="form-group">
            <label for="period">Chu kỳ</label>
            <input type="text" id="period" name="import_period" value="<?=date('m-Y')?>" class="form-control" placeholder="tháng-năm">
            <input type="button" id="previous-month" class="btn btn-default" value="Tháng trước">
            <input type="button" id="next-month" class="btn btn-default" value="Tháng sau">
            <p class="help-block">Nhấp vào nút <b>Tháng trước</b>, <b>Tháng sau</b> để thay đổi nhanh Chu Kỳ của tập tin xử lý.</p>
          </div>
          <div class="form-group">
            <label>Tập tin</label><input type="file" name="doc" />
            <p class="help-block">
              Tập tin Excel phải tuân thủ đúng cấu trúc tiêu đề cột như bên dưới:<br/>
              <strong>Cột 1</strong><span>So TB</span><br/>
              <strong>Cột 2</strong><span>Ten TB</span><br/>
              <strong>Cột 3</strong><span>Dia Chi</span><br/>
              <strong>Cột 4</strong><span>MST</span><br/>
              <strong>Cột 5</strong><span>CK</span><br/>
              <strong>Cột 6</strong><span>Số Tiền</span><br/><br/>
              <b class="text-danger">Lưu ý:</b><br/>
              <span class="text-danger">Nếu một sheet mà cột không có tiêu đề thì sẽ báo lỗi và không lưu dữ liệu của sheet đó.</span><br/>
              <span class="text-danger">Nếu một sheet mà tiêu đề cột 5 là Số Tiền, thì Chu Kỳ (CK) sẽ được hiểu là 1.</span><br/>
            </p>
          </div>
          <div class="form-group">
            <input type="submit" name="upload" value="Tải lên" class="btn btn-primary" />
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
<br>
