<?php
require_once(LIBPATH . 'PHPExcel/PHPExcel.php');
require_once(LIBPATH . "tcpdf/tcpdf.php");

if (isset($_POST["upload"])) {
  // Upload file
  $upload_dir = FILEPATH . '/public/' . date('YmdHis') . '_' . generate_random_string();
  mkdir($upload_dir, '0777');
  $file_path = $upload_dir . '/' . $_FILES['doc']['name'];
  $upload = upload_file($file_path);
  if ($upload === true) {
//    // Set up PDF
//    $pdf = new TCPDF('L', PDF_UNIT, null, true, 'UTF-8', false);
//    $pdf->SetFont('dejavusans', '', 14, '', true);
//    $pdf->setPrintHeader(false);
//    $pdf->setPrintFooter(false);
//    $pdf->SetMargins(8, 8, 8);
    
//    // Load template
//    ob_start();
//    include_once(FILEPATH . '/templates/' . 'test.html');
//    $template = ob_get_contents();
//    ob_end_clean();
    
    // Read file
//    $html = '';
    $count = 0;
    $objPHPExcel = PHPExcel_IOFactory::load($file_path);
    foreach ($objPHPExcel->getWorksheetIterator() as $k => $worksheet) {
      if ($k == 0) continue;
      $worksheet_title = $worksheet->getTitle();
      $highest_row = $worksheet->getHighestRow();
      $highest_column = $worksheet->getHighestColumn();
      $highest_column_index = PHPExcel_Cell::columnIndexFromString($highest_column);

      $error = array();
      // Check if sheet columns are valid
      if (trim($worksheet->getCellByColumnAndRow(1, 1)->getValue()) != 'So TB'
      || trim($worksheet->getCellByColumnAndRow(2, 1)->getValue()) != 'Ten TB'
      || trim($worksheet->getCellByColumnAndRow(3, 1)->getValue()) != 'Dia Chi'
      || trim($worksheet->getCellByColumnAndRow(4, 1)->getValue()) != 'MST'
      || trim($worksheet->getCellByColumnAndRow(5, 1)->getValue()) != 'CK'
      || trim($worksheet->getCellByColumnAndRow(6, 1)->getValue()) != 'Số Tiền') {
        $error[] = "Sheet {$worksheet_title} không đúng cấu trúc cột.";
        continue;
      }

//      $html .= '<h3>' . $worksheet_title . '</h3>';
//      $html .= '<table>';
//      $html .= '<tr>
//          <td>So TB</td>
//          <td>Ten TB</td>
//          <td>Dia chi</td>
//          <td>MST</td>
//          <td>Chu ky</td>
//          <td>So tien</td>
//        </tr>';
      for ($row = 3; $row <= $highest_row; $row++) {
        $i = 1;
        $phone_no = trim($worksheet->getCellByColumnAndRow($i++, $row)->getValue());
        $owner = trim($worksheet->getCellByColumnAndRow($i++, $row)->getValue());
        $address = trim($worksheet->getCellByColumnAndRow($i++, $row)->getValue());
        $tax_code = trim($worksheet->getCellByColumnAndRow($i++, $row)->getValue());
        $period = trim($worksheet->getCellByColumnAndRow($i++, $row)->getValue());
        $amount = trim($worksheet->getCellByColumnAndRow($i++, $row)->getValue());

        // If number and name is empty, skip row
        if (empty($phone_no) || empty($owner) || empty($amount)) {
          continue;
        }

        $query = "INSERT INTO import
                  SET
                    phone_no = '" . escape($phone_no) . "',
                    owner = '" . escape($owner) . "',
                    address = '" . escape($address) . "',
                    tax_code = '" . escape($tax_code) . "',
                    period = '" . escape($period) . "',
                    amount = '" . escape($amount) . "'";
        $mysqli->query($query);

//        $html .= '<tr>';
//        $html .= '<td>' . $phone_no . '</td>';
//        $html .= '<td>' . $owner . '</td>';
//        $html .= '<td>' . $address . '</td>';
//        $html .= '<td>' . $tax_code . '</td>';
//        $html .= '<td>' . $period . '</td>';
//        $html .= '<td>' . $amount . '</td>';
//        $html .= '</tr>';
//
//        // Write content
//        $content = $template;
//        $content = str_replace('{{OWNER}}', $owner, $content);
//        $content = str_replace('{{PHONE_NO}}', $phone_no, $content);
//        $content = str_replace('{{ADDRESS}}', $address, $content);
//        $content = str_replace('{{TAX_CODE}}', $tax_code, $content);
//        $content = str_replace('{{PERIOD}}', $period, $content);
//        $content = str_replace('{{AMOUNT}}', $amount, $content);
//        $pdf->AddPage();
//        $pdf->writeHTML($content);
      }
//      $html .= '</table>';
  
//      $pdf->Output(FILEPATH . '/public/' . 'example_001.pdf', 'F');

      // Show missing data rows
      if ($error) {
        array_unshift($error, "<b>Sheet $worksheet_title</b>");
        error(implode('<br>', $error));
      }
      break;
    }
  } else {
    error($upload);
  }
}

?>

<div class="row">
  <div class="col-md-12">
    <h1>Importer</h1>
    <?=display_msg()?>
    <form action="" method="post" enctype="multipart/form-data">
      <div class="panel panel-default">
        <div class="panel-body">
          <div class="form-group">
            <label>Tập tin</label><input type="file" name="doc" />
            <p class="help-block">
              Lưu ý, tập tin Excel phải tuân thủ đúng cấu trúc như bên dưới:<br/>
              <strong>Cột 1</strong><span>Số thuê bao</span><br/>
              <strong>Cột 2</strong><span>Tên thuê bao</span><br/>
              <strong>Cột 3</strong><span>Địa chỉ</span><br/>
              <strong>Cột 4</strong><span>Mã số thuế</span><br/>
              <strong>Cột 5</strong><span>Chu kỳ</span><br/>
              <strong>Cột 6</strong><span>Số tiền</span>
            </p>
          </div>
          <div class="form-group">
            <input type="submit" name="upload" value="Tải lên" class="btn btn-primary" />
          </div>
        </div>
      </div>
    </form>
  </div>
</div>

<br>
<?=isset($html) ? $html : '';?>
<br>
