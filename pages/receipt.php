<?php
$rows = array();
if (isset($_GET['phone'])) {
    $phone = !empty($_GET['phone']) ? escape($_GET['phone']) : '';
    if ($phone) {
        $mysqli = MyDB::getInstance()->getConnection();
        $query = "SELECT * FROM `import` WHERE phone_no = '{$phone}'";
        $result = $mysqli->query($query);
        if (!empty($result)) {
            while ($data = mysqli_fetch_assoc($result)) {
                $rows[] = $data;
            }
        }

        if (!$rows) {
            info('Không tìm thấy dữ liệu.', MODE_ERROR);
        }
    } else {
        info('Vui lòng nhập số thuê bao.', MODE_ERROR);
    }
}
?>

<link href="/css/receipt.css" rel="stylesheet">
<div class="row">
    <div class="col-md-12">
        <h1 class="text-primary">Hóa đơn</h1>
        <?= display_msg() ?>
    </div>
    <div class="col-md-3">
        <form action="" method="get" class="form-inline">
            <div id="search-box" class="panel panel-default">
                <div class="panel-body">
                    <label for="phone">Số thuê bao</label>
                    <div class="form-group">
                        <input type="text" id="phone" name="phone" class="form-control"
                               value="<?= !empty($_GET['phone']) ? $_GET['phone'] : ''; ?>"/>
                        <input type="submit" id="search" value="Tìm" class="btn btn-primary"/>
                    </div>
                </div>
            </div>
        </form>

        <?php if ($rows) { ?>
            <div id="search-result" class="panel panel-default">
                <div class="panel-body">
                    <?php foreach ($rows as $k => $data) { ?>
                        <span class="<?= $k == 0 ? 'active' : '' ?>"
                              data-for="receipt-<?= $k + 1 ?>"><?= ($k + 1) . '. ' . $data['phone_no'] . ' - ' . format_money($data['amount']) ?></span>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>
    </div>

    <div class="col-md-9">
        <div class="print-region">
            <?php foreach ($rows as $k => $data) {
                $owner_style = strlen($data['owner']) > 100 ? 'two-lines' : 'one-line';
                $address_style = strlen($data['address']) > 110 ? 'two-lines' : 'one-line';
                $date = new DateTime($data['date']);
                $p_start = date('Y-m-', strtotime($data['date'] . ' - 1 month')) . str_pad($data['period'], 2, '0', STR_PAD_LEFT);
                $stt_start = strtotime($p_start);
                $period_start = date('d', $stt_start) . str_repeat('&nbsp;', 4);
                $period_start .= date('m', $stt_start) . str_repeat('&nbsp;', 8);
                $period_start .= date('y', $stt_start);
                $p_end = date('Y-m-d', strtotime($p_start . ' + 1 month - 1 day'));
                $stt_end = strtotime($p_end);
                $period_end = date('d', $stt_end) . str_repeat('&nbsp;', 4);
                $period_end .= date('m', $stt_end) . str_repeat('&nbsp;', 8);
                $period_end .= date('y', $stt_end);
                $group_total = $data['amount'];
                $amount = $total = $group_total / 1.1;
                $tax = $total * 10 / 100;
                ?>
                <div id="receipt-<?= $k + 1 ?>" class="page-a4 <?= $k != 0 ? 'hidden' : '' ?>" data-index="<?= $k ?>"
                     data-period-start="<?php echo date('d/m/y', $stt_start); ?>"
                     data-period-end="<?php echo date('d/m/y', $stt_end); ?>">
                    <span id="owner" class="<?= $owner_style ?>"><?= $data['owner'] ?></span>
                    <span id="tax-code"><?= display_tax($data['tax_code']) ?></span>
                    <span id="address" class="<?= $address_style ?>"><?= $data['address'] ?></span>
                    <span id="phone-no"><?= $data['phone_no'] ?></span>
                    <span id="period-start" class="period"><?= $period_start ?></span>
                    <span id="period-end" class="period"><?= $period_end ?></span>
                    <span id="amount" class="align-right"><?= format_money($amount) ?></span>
                    <span id="total" class="align-right"><?= format_money($total) ?></span>
                    <span id="tax">10</span>
                    <span id="tax-amount" class="align-right"><?= format_money($tax) ?></span>
                    <span id="group-total" class="align-right"><?= ($money = format_money($group_total)) ?></span>
                    <span id="words-total"><?= money_to_words($money) ?></span>
                    <span id="print-day" class="print-date"><?= date('d') ?></span>
                    <span id="print-month" class="print-date"><?= date('m') ?></span>
                    <span id="print-year" class="print-date"><?= date('y') ?></span>
                    <span id="name">Đinh Quý Thành</span>
                </div>
            <?php } ?>
            <?php if (!empty($rows)) { ?>
                <div class="btn-wrapper">
                    <input type="button" id="next-receipt" class="btn btn-default" value="Hóa đơn sau">
                    <input type="button" id="print-receipt" name="print" value="In hóa đơn" class="btn btn-success"/>
                    <input type="button" id="previous-receipt" class="btn btn-default" value="Hóa đơn trước">
                    <input type="button" id="change-period" value="Thay đổi chu kỳ" class="btn btn-info"
                           data-toggle="modal" data-target="#period-modal"/>
                    <div class="clear"></div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>

<div id="period-modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Thay đổi chu kỳ</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="new-period-start" class="control-label">Ngày bắt đầu:</label>
                            <input type="text" class="form-control" id="new-period-start">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="new-period-end" class="control-label">Ngày kết thúc:</label>
                            <input type="text" class="form-control" id="new-period-end">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                <button type="button" id="btn-change-period" class="btn btn-primary">Lưu</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script src="/js/jquery.PrintArea.js"></script>