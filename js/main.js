$(document).ready(function () {
    // Change to previous month
    $('#previous-month').click(function () {
        var period = $('#period').val();
        var pattern = /(\d{2})\-(\d{4})/;
        var newPeriod = new Date(+new Date(period.replace(pattern, '$2-$1-01 00:00:00')) - (31 * 24 * 60 * 60 * 1000));
        var mm = newPeriod.getMonth() + 1;
        mm = mm >= 10 ? mm : !mm[1] && '0' + mm;
        var yyyy = newPeriod.getFullYear();
        $('#period').val(mm + '-' + yyyy);
    });

    // Change to next month
    $('#next-month').click(function () {
        var period = $('#period').val();
        var pattern = /(\d{2})\-(\d{4})/;
        var newPeriod = new Date(+new Date(period.replace(pattern, '$2-$1-01 00:00:00')) + (31 * 24 * 60 * 60 * 1000));
        var mm = newPeriod.getMonth() + 1;
        mm = mm >= 10 ? mm : !mm[1] && '0' + mm;
        var yyyy = newPeriod.getFullYear();
        $('#period').val(mm + '-' + yyyy);
    });

    // When clicking print button
    $('#print-receipt').click(function () {
        $('.page-a4').printArea();
    });

    // Default status of current receipt
    changeReceipt();

    // When clicking on search result
    $('#search-result span').click(function () {
        $('#search-result span').removeClass('active');
        $(this).addClass('active');
        var forId = $(this).attr('data-for');
        $('.page-a4').addClass('hidden');
        $('#' + forId).removeClass('hidden');
        changeReceipt();
    });

    // When clicking on next receipt
    $('#next-receipt').click(function () {
        var current = $('.page-a4:not(.hidden)').data('index');
        $('.page-a4').addClass('hidden');
        $('.page-a4[data-index=' + (current + 1) + ']').removeClass('hidden');
        changeReceipt();
    });

    // When clicking on previous receipt
    $('#previous-receipt').click(function () {
        var current = $('.page-a4:not(.hidden)').data('index');
        $('.page-a4').addClass('hidden');
        $('.page-a4[data-index=' + (current - 1) + ']').removeClass('hidden');
        changeReceipt();
    });

    // Pre-fill period dates
    $('#period-modal').on('shown.bs.modal', function (e) {
        var currentReceipt = $('.page-a4:visible');
        $('#new-period-start').val(currentReceipt.data('period-start'));
        $('#new-period-end').val(currentReceipt.data('period-end'));
    });

    // Update new period dates
    $('#btn-change-period').click(function () {
        var tab = repeat('&nbsp;', 4);
        var newPeriodStart = $('#new-period-start').val();
        var newPeriodEnd = $('#new-period-end').val();
        var start = newPeriodStart.replace('/', tab).replace('/', tab + tab);
        var end = newPeriodEnd.replace('/', tab).replace('/', tab + tab);

        var currentReceipt = $('.page-a4:visible');
        currentReceipt.find('#period-start').html(start);
        currentReceipt.find('#period-end').html(end);
        
        $('#period-modal').modal('hide');
    });
});

/**
 * Handling status of current receipt
 */
function changeReceipt() {
    var current = $('.page-a4:not(.hidden)').data('index');
    if (current == 0) {
        $('#previous-receipt').prop('disabled', true);
    } else {
        $('#previous-receipt').prop('disabled', false);
    }

    if (current == $('.page-a4').length - 1) {
        $('#next-receipt').prop('disabled', true);
    } else {
        $('#next-receipt').prop('disabled', false);
    }

    $('#search-result span').removeClass('active');
    $('#search-result span:eq(' + current + ')').addClass('active');
}

function repeat(pattern, count) {
    if (count < 1) return '';
    var result = '';
    while (count > 1) {
        if (count & 1) result += pattern;
        count >>= 1, pattern += pattern;
    }
    return result + pattern;
}