<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Importer</title>
  
  <!-- Bootstrap -->
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <link href="/css/bootstrap-theme.min.css" rel="stylesheet">
  <link href="/css/bootstrap-custom.css" rel="stylesheet">
  <link href="/css/font-awesome.min.css" rel="stylesheet">
  <link href="/css/main.css" rel="stylesheet">
    
  <script src="/js/jquery.min.js"></script>
</head>

<body>
	<?php require_once('navigation.php'); ?>
	
	<div class="content container">
    <?php
      $page_file = PAGEPATH . str_replace('-', '_', strtolower($page)).'.php';
      if (file_exists($page_file)) {
        require_once($page_file);
      } else {
        echo '<h1>Page not found.</h1>';
      }
    ?>
    
	</div>
  
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="/js/main.js"></script>
</body>
</html>