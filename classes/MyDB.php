<?php

/**
 * Created by PhpStorm.
 * User: duc
 * Date: 14/07/2016
 * Time: 17:40
 */
class MyDB
{
    /**
     * @var $instance MyDB Database instance
     */
    private static $instance;
    /**
     * @var $host string Database host
     */
    private $host;
    /**
     * @var $username string Database username
     */
    private $username;
    /**
     * @var $password string Database password
     */
    private $password;
    /**
     * @var $database string Database name
     */
    private $database;

    /**
     * @var $connection mysqli Database connection
     */
    private $connection;

    /**
     * Initial database connection 
     * @throws Exception
     */
    private function __construct() {
        $this->host = getenv('db_host');
        $this->username = getenv('db_user');
        $this->password = getenv('db_pass');
        $this->database = getenv('db_name');

        if (empty($this->host) || empty($this->username) || empty($this->password) || empty($this->database)) {
            throw new Exception('Could not connect database.');
        }
        $this->connection = new mysqli($this->host, $this->username, $this->password, $this->database);
        if (mysqli_connect_error()) {
            throw new Exception('Could not connect database.');
        }
    }

    /**
     * Prevent clone new database instance
     */
    private function __clone() {}

    /**
     * Generate Singleton DB instance
     * 
     * @return MyDB
     */
    public static function getInstance() {
        if (self::$instance === null) {
            self::$instance = new MyDB();
        }
        
        return self::$instance;
    }

    /**
     * Get database connection
     * @return mysqli
     */
    public function getConnection() {
        return $this->connection;
    }
}