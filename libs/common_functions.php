<?php

function escape($value) {
  $mysqli = MyDB::getInstance()->getConnection();
  return $mysqli->escape_string($value);
}

function error($val) {
  $_SESSION['err'][] = $val;
}

function success($val) {
  $_SESSION['msg'][] = $val;
}

function info($val, $mode = MODE_PRIMARY) {
  $_SESSION['info'][] = '<span class="text-' . $mode . '">' . $val . '</span>';
}

function display_msg() {
  $msg = '';
  if (isset($_SESSION['msg']) && is_array($_SESSION['msg'])) {
    $msg .= '<div class="alert alert-success">' . implode('<br>', $_SESSION['msg']) . '</div>';
  }
  if (isset($_SESSION['err']) && is_array($_SESSION['err'])) {
    $msg .= '<div class="alert alert-danger">' . implode('<br>', $_SESSION['err']) . '</div>';
    
  }
  if (isset($_SESSION['info']) && is_array($_SESSION['info'])) {
    $msg .= '<div class="alert alert-info">' . implode('<br>', $_SESSION['info']) . '</div>';
  }
  if (isset($_SESSION['debug']) && is_array($_SESSION['debug'])) {
    $msg .= '<div class="panel-group panel-debug" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-info">' . implode('', $_SESSION['debug']) . '</div>
    </div>';
  }
  unset($_SESSION['msg']);
  unset($_SESSION['err']);
  unset($_SESSION['info']);
  unset($_SESSION['debug']);
  return $msg;
}

function debug($var, $title = '') {
  $prefix = $title ? '<i style="display:block;">' . $title . '</i>' : '';
  $id = md5(microtime() . rand());
  $_SESSION['debug'][] = '
    <div class="panel-heading" role="tab" id="heading' . $id . '">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse' . $id . '"
          aria-expanded="true" aria-controls="collapse' . $id . '">
          ' . $prefix . '
        </a>
      </h4>
    </div>
    <div id="collapse' . $id . '" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
        ' . print_r($var, TRUE) . '
      </div>
    </div>
  ';
}

function generate_random_string($length = 10) {
  $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  $characters_length = strlen($characters);
  $randomString = '';
  for ($i = 0; $i < $length; $i++) {
    $randomString .= $characters[rand(0, $characters_length - 1)];
  }
  return $randomString;
}

function upload_file($file_path) {
  try {
    if (!isset($_FILES['doc']['error']) || is_array($_FILES['doc']['error'])) {
      throw new Exception('Invalid parameters.');
    }

    // Check $_FILES['doc']['error'] value.
    switch ($_FILES['doc']['error']) {
      case UPLOAD_ERR_OK:
        break;
      case UPLOAD_ERR_NO_FILE:
        throw new RuntimeException('No file sent.');
      case UPLOAD_ERR_INI_SIZE:
      case UPLOAD_ERR_FORM_SIZE:
        throw new Exception('Exceeded filesize limit.');
      default:
        throw new Exception('Unknown errors.');
    }

    // Upload file
    if (!move_uploaded_file($_FILES['doc']['tmp_name'], $file_path)) {
      throw new Exception('Failed to move uploaded file.');
    }
    return true;
  } catch (Exception $e) {
    return $e->getMessage();
  }
}

function formatted_number_to_float($number) {
  $number = str_replace($number, '.', '');
  $number = str_replace($number, ',', '.');
  return floatval($number);
}

function display_tax($tax_code) {
  $tax_code = str_split($tax_code);
  array_splice($tax_code, 2, 0, '');
  array_splice($tax_code, 10, 0, '');
  array_splice($tax_code, 12, 0, '');
  array_splice($tax_code, 16, 0, '');
  array_walk($tax_code, 'wrap_tax');
  return implode('', $tax_code);
}

function wrap_tax(&$tax_code) {
  $tax_code = $tax_code !== '' ? '<span class="tax-code">' . $tax_code . '</span>' : '<span class="tax-separate">&nbsp;</span>';
}

function format_money($value) {
  return number_format($value, 0, '.', ',');
}

function number_to_money_words($value) {
  $money = floatval(str_replace(',', '', $value));
  $money = round($money);
  return money_to_words($money);
}

function money_to_words($money)
{
  $parts = explode(',', $money);
  if ($count = count($parts) > 4) return '###';
  else {
    $parts = array_reverse($parts);
    $parts = array_reverse(array_pad($parts, 4, '000'));
  }

  $result = '';
  $arr = array('tỷ', 'triệu', 'ngàn', '');
  for ($i = 0; $i <= 3; $i++) {
    $words = hundred_to_words($parts[$i], empty($result));
    if ($words) {
      $result .= $result ? ', ' : '';
      $result .= $words . ' ' . $arr[$i];
    }
    if ($i == 3) {
      $result .= ' đồng';
    }
  }
  $result = trim(preg_replace('/\s\s+/', ' ', str_replace("\n", " ", $result)));
  return ucfirst($result);
}

function hundred_to_words($number, $no_hundred = false) {
  $text = array(
    0 => 'không',
    1 => 'một',
    2 => 'hai',
    3 => 'ba',
    4 => 'bốn',
    5 => 'năm',
    6 => 'sáu',
    7 => 'bảy',
    8 => 'tám',
    9 => 'chín',
  );
  $number = str_pad($number, 3, 0, STR_PAD_LEFT);
  if ($number === '000') return '';

  $a = substr($number, 0, 1);
  $b = substr($number, 1, 1);
  $c = substr($number, 2, 1);

  if ($no_hundred) {
    // Do not return "không trăm"
    $text_a = $a ? $text[$a] . ' trăm' : '';
  } else {
    // Always return "không trăm"
    $text_a = $text[$a] . ' trăm';
  }
  $text_b = $b ? ' ' . $text[$b] . ' mươi' : '';
  $text_c = $c ? ' ' . $text[$c] : '';
  $text = trim($text_a . $text_b . $text_c);

  // Replace text for special case
  if (!$text_b) {
    $text = str_replace('trăm ', 'trăm lẻ ', $text);
  } else {
    $text = str_replace('một mươi', 'mười', $text);
    $text = str_replace('mười năm', 'mười lăm', $text);
    $text = str_replace('mươi một', 'mươi mốt', $text);
    $text = str_replace('mươi năm', 'mươi lăm', $text);
  }
  return $text;
}
