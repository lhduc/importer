<?php
ini_set("display_errors", "1");
error_reporting(E_ALL);

// Add constants
require_once('vendor/autoload.php');
require_once('constants.php');
require_once(LIBPATH . 'common_functions.php');

// Load environment config
$env = new \Dotenv\Dotenv(__DIR__, '.env');
$env->load();

session_start();

$base_url = 'http://' . $_SERVER['HTTP_HOST'] . '/';
if (isset($_GET['page']) && strpos($_GET['page'], 'ajax/') === 0) {
  // AJAX page
  $page = str_replace('ajax/', '', $_GET['page']);
  $page = str_replace('-', '_', $page);
  if (file_exists($page . '.php')) {
    require_once(PAGEPATH . $page . '.php');
  }
} else {
  // Normal page
  $page = !empty($_GET['page']) ? str_replace('/', '', $_GET['page']) : 'home';
  require_once('template.php');
}


